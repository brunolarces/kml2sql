package com.blogspot.kariridev.kml;

import java.util.Iterator;

import javax.xml.XMLConstants;
import javax.xml.namespace.NamespaceContext;

public class KMLNamespaceContext implements NamespaceContext {

	public String getNamespaceURI(String prefix) {

		if (prefix == null)
			throw new NullPointerException("Null prefix");
		else if ("kml".equals(prefix))
			return "http://earth.google.com/kml/2.1";
		else if ("xml".equals(prefix))
			return XMLConstants.XML_NS_URI;
		return XMLConstants.NULL_NS_URI;
	}

	public String getPrefix(String namespaceURI) {
		throw new UnsupportedOperationException();
	}

	@SuppressWarnings("rawtypes")
	public Iterator getPrefixes(String namespaceURI) {
		throw new UnsupportedOperationException();
	}

}
