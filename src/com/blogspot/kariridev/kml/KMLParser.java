package com.blogspot.kariridev.kml;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class KMLParser {

	/**
	 * Parse the KML file
	 * 
	 * @param in
	 *            The {@link InputStream} of KML file
	 * @return a {@link List} of {@link Layer} with {@link Placemark}.
	 */
	public static List<String> parse(InputStream in) {

		// Create the sqls array
		List<String> sqlList = null;

		// Create the XPath Object and set the namespace context
		XPath xPath = XPathFactory.newInstance().newXPath();
		xPath.setNamespaceContext(new KMLNamespaceContext());

		try {

			// Create a InputSource object
			InputSource source = new InputSource(in);

			// Retrieve the NodeList object before evaluate
			NodeList layers = (NodeList) xPath.evaluate(
					"/kml:kml/kml:Document/kml:Folder", source,
					XPathConstants.NODESET);

			/*
			 * If the nodes list is greater than 0...
			 */
			if (layers.getLength() > 0) {

				// Instantiate the layers list
				sqlList = new ArrayList<>();

				// Iterate over the NodeList object
				for (int i = 0; i < layers.getLength(); i++) {

					// Retrieve the node
					Node layer = layers.item(i);

					// Get the name attribute
					String name = ((Node) xPath.evaluate("kml:name", layer,
							XPathConstants.NODE)).getTextContent();

					// Create the layer sql insert
					sqlList.add("INSERT INTO layers (name) VALUES ('" + name
							+ "');");

					// Get the placemarks of the node
					NodeList placemarks = ((NodeList) xPath.evaluate(
							"kml:Placemark", layer, XPathConstants.NODESET));

					/*
					 * If placemarks length is greater than 0...
					 */
					if (placemarks.getLength() > 0) {

						// Iterate over the NodeList object
						for (int j = 0; j < placemarks.getLength(); j++) {

							// Retrieve the node
							Node placemark = placemarks.item(j);

							// Get the description attribute
							String description = ((Node) xPath.evaluate(
									"kml:description", placemark,
									XPathConstants.NODE)).getTextContent();

							// Get the LookAt of the placemark
							Node lookat = ((Node) xPath.evaluate("kml:LookAt",
									placemark, XPathConstants.NODE));

							// Get the longitude attribute
							String longitude = ((Node) xPath.evaluate(
									"kml:longitude", lookat,
									XPathConstants.NODE)).getTextContent();

							// Get the latitude attribute
							String latitude = ((Node) xPath
									.evaluate("kml:latitude", lookat,
											XPathConstants.NODE))
									.getTextContent();

							// Get the heading attribute
							String heading = ((Node) xPath.evaluate(
									"kml:heading", lookat, XPathConstants.NODE))
									.getTextContent();

							// Get the range attribute
							String range = ((Node) xPath.evaluate("kml:range",
									lookat, XPathConstants.NODE))
									.getTextContent();

							// Remove the html markup of description
							description = description.replace("<b>", "");
							description = description.replace("</b>", "");
							description = description.replace("<br/>", "");
							description = description.trim();

							// Create the placemark sql insert
							sqlList.add("INSERT INTO placemarks (description,longitude,latitude,heading,range,layer) VALUES ('"
									+ description
									+ "', "
									+ longitude
									+ ","
									+ latitude
									+ ","
									+ heading
									+ ","
									+ range
									+ ", " + (i + 1) + ");");

						}

					}

				}

			}

			in.close();

		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return sqlList;

	}

}
