package com.blogspot.kariridev;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import com.blogspot.kariridev.kml.KMLParser;

public class App {

	public static void main(String[] args) {

		try {

			System.out.println("Iniciado o processo de conversão.");
			
			File kml = new File("/home/bruno/Documentos/Radares/pontos.kml");
			FileInputStream in = new FileInputStream(kml);

			List<String> sqlArray = KMLParser.parse(in);
			
			File sql = new File("/home/bruno/Documentos/Radares/droidradar.sql");
			
			
			if(sql.exists()){
				sql.delete();
			}
			
			sql.createNewFile();
			
			PrintWriter printWriter = new PrintWriter(sql); 
			
			for (String string : sqlArray) {
				printWriter.println(string);
			}
			
			System.out.println("Terminado o processo de conversão.");

		} catch (FileNotFoundException e) {

			e.printStackTrace();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
